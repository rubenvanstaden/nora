#ifndef ABC_H
#define ABC_H

#include "sys.h"
#include "grid.h"

class Abc
{
    public:
        int init_done;
        double coef0, coef1, coef2;
        double *ezLeft, ezRight, *ezTop, *ezBottom;
        
        arma::mat EzLeft_1;
        arma::mat EzRight_1;
        arma::mat EzTop_1;
        arma::mat EzBottom_1;
        
        arma::mat EzLeft_2;
        arma::mat EzRight_2;
        arma::mat EzTop_2;
        arma::mat EzBottom_2;
        
        Abc();
        ~Abc();
        
        void abc_init(System *sys, Grid *g);
        void abc(System *sys, Grid *g);
};

#endif