#ifndef UPDATE_H 
#define UPDATE_H

#include "sys.h"
#include "grid.h"

void updateE(System *sys, Grid *g, Grid *g1);
void updateH(System *sys, Grid *g, Grid *g1);

#endif