#ifndef TFSF_H
#define TFSF_H

#include "grid.h"
#include "sources.h"
#include "update.h"

class Tfsf
{
    public:        
        double firstX, firstY;
        double lastX, lastY;
        
        Tfsf();
        ~Tfsf();
        
        void tfsf_init(System *sys, Grid *g);
        void tfsf_update(System *sys, Grid *g, Grid *g1, Sources *source);
};

#endif