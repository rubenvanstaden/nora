#ifndef VTK_TOOLS_H
#define VTK_TOOLS_H

void vtk_ez(System *sys, Grid *grid);
void test();
void test_surface(System *sys, Grid *grid);
void vtk_line_plot(System *sys, Grid *grid);

# endif