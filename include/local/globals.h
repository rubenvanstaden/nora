#ifndef GLOBALS_H
#define GLOBALS_H

#include <math.h>

const double IMP0 = 377.0;
// const double CDTDS = 1.0 / std::sqrt(2.0);
const double CDTDS = 1.0;

const double NLOSS = 20;
const double MAX_LOSS = 0.35;

const double PI = 3.14159265358979323846;
const double UM = 10e-12;
const double MU = 4 * PI * 10e-7;
const double C = MU / (4 * PI);

#endif