#ifndef SOURCES_H
#define SOURCES_H

#include <math.h>
#include <iostream>

#include "sys.h"
#include "globals.h"

class Sources
{
    public:
        double ppw;
        arma::vec ricker;
        
        Sources();
        ~Sources();
        
        double ricker_wave(int t, double location);
        void init_source(System *sys);
};

#endif