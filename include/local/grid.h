#ifndef GRAPH_H
#define GRAPH_H

#include <stddef.h>
#include <armadillo>

class Grid
{
    public:
        int w;
        int h;
        int time;
        int type;

        int firstX;
        int firstY;
        int lastX;
        int lastY;
                
        // 1D
        arma::vec Hx1D;
        arma::vec Hy1D;
        arma::vec Ez1D;
        
        arma::vec Ceze1D;
        arma::vec Cezh1D;
        
        arma::vec Chxe1D;
        arma::vec Chxh1D;
        
        arma::vec Chye1D;
        arma::vec Chyh1D;
        
        // 2D
        arma::mat Hx;
        arma::mat Hy;
        arma::mat Ez;
        
        arma::mat Ceze;
        arma::mat Cezh;
        
        arma::mat Chxe;
        arma::mat Chxh;
        
        arma::mat Chye;
        arma::mat Chyh;
        
        Grid();
        ~Grid();
        
        void init_grid();
        void init_grid_1D();
};

#endif

















