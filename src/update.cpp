#include "sys.h"
#include "grid.h"

void updateH(System *sys, Grid *g, Grid *g1)
{
    // Het nie die fokken array geupdate nie
    arma::mat Ez = g->Ez;
    arma::mat Hx = g->Hx;
    arma::mat Hy = g->Hy;
    
    // arma::mat Chxe = g->Chxe;
    // arma::mat Chxh = g->Chxh;
    // arma::mat Chye = g->Chye;
    // arma::mat Chyh = g->Chyh;
    // 
    arma::mat Ez1D = g1->Ez1D;
    arma::mat Hx1D = g1->Hx1D;
    arma::mat Hy1D = g1->Hy1D;
    
    arma::mat Chxe1D = g1->Chxe1D;
    arma::mat Chxh1D = g1->Chxh1D;
    arma::mat Chye1D = g1->Chye1D;
    arma::mat Chyh1D = g1->Chyh1D;

    if (g->type == 1) {
        for (int i = 0; i < g1->w-1; i++) {
            g1->Hy1D(i) = g1->Chyh1D(i) * g1->Hy1D(i) + g1->Chye1D(i) * (g1->Ez1D(i+1) - g1->Ez1D(i));
        }
    } else {
        for (int i = 0; i < g->w; i++) {
            for (int j = 0; j < g->h-1; j++) {
                g->Hx(i,j) = g->Chxh(i,j) * Hx(i,j) - g->Chxe(i,j) * (Ez(i,j+1) - Ez(i,j));
            }
        }
        
        for (int i = 0; i < g->w-1; i++) {
            for (int j = 0; j < g->h; j++) {
                g->Hy(i,j) = g->Chyh(i,j) * Hy(i,j) - g->Chye(i,j) * (Ez(i+1,j) - Ez(i,j));
            }
        }    
    }
}

void updateE(System *sys, Grid *g, Grid *g1)
{
    arma::mat Ez = g->Ez;
    // arma::mat Hx = g->Hx;
    // arma::mat Hy = g->Hy;
    // 
    // arma::mat Ceze = g->Ceze;
    // arma::mat Cezh = g->Cezh;
    
    // arma::mat Ez1D = g1->Ez1D;
    // arma::mat Hy1D = g1->Hy1D;
    // 
    // arma::mat Ceze1D = g1->Ceze1D;
    // arma::mat Cezh1D = g1->Cezh1D;
    
    if (g->type == 1) {
        for (int i = 1; i < g1->w-1; i++) {
            g1->Ez1D(i) = g1->Ceze1D(i) * g1->Ez1D(i) + g1->Cezh1D(i) * (g1->Hy1D(i) - g1->Hy1D(i-1));
        }
    } else {
        for (int i = 1; i < g->w-1; i++) {
            for (int j = 1; j < g->h-1; j++) {
                g->Ez(i,j) = g->Ceze(i,j) * Ez(i,j) + g->Cezh(i,j) * ((g->Hy(i,j) - g->Hy(i-1,j)) - (g->Hx(i,j) - g->Hx(i,j-1)));
            }
        }    
    }
}























