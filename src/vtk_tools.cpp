#include <vtkVersion.h>
#include <vtkSmartPointer.h>
#include <vtkXMLStructuredGridWriter.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkXMLPolyDataReader.h>

#include <vtkPolyDataMapper.h>
#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkArrayCalculator.h>
#include <vtkDoubleArray.h>
#include <vtkPointData.h>
#include <vtkAppendFilter.h>
#include <vtkPolygon.h>
#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkProperty.h>
#include <vtkStructuredGrid.h>
#include <vtkDataSetMapper.h>
#include <vtkPolyhedron.h>
#include <vtkWedge.h>
#include <vtkPointSource.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkPolyDataReader.h>

#include "sys.h"
#include "grid.h"





#include "vtkChartXYZ.h"
#include "vtkContextMouseEvent.h"
#include "vtkContextView.h"
#include "vtkContextScene.h"
#include "vtkFloatArray.h"
#include "vtkNew.h"
#include "vtkPlotPoints3D.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkTable.h"
#include "vtkUnsignedCharArray.h"
#include "vtkVector.h"

#ifdef vtkGenericDataArray_h
#define InsertNextTupleValue InsertNextTypedTuple
#endif

#include "vtkChartXYZ.h"
#include "vtkContextMouseEvent.h"
#include "vtkContextView.h"
#include "vtkContextScene.h"
#include "vtkFloatArray.h"
#include "vtkNew.h"
#include "vtkPlotSurface.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkTable.h"
#include "vtkUnsignedCharArray.h"
#include "vtkVector.h"

void test_surface(System *sys, Grid *grid)
{
    vtkNew<vtkChartXYZ> chart;
    vtkNew<vtkPlotSurface> plot;
    vtkNew<vtkContextView> view;
    view->GetRenderWindow()->SetSize(400, 300);
    view->GetScene()->AddItem(chart.GetPointer());

    chart->SetGeometry(vtkRectf(75.0, 20.0, 250, 260));

    vtkNew<vtkTable> table;
    for (vtkIdType i = 0; i < grid->w; i++) {
        vtkNew<vtkFloatArray> arr;
        table->AddColumn(arr.GetPointer());
    }
    
    table->SetNumberOfRows(static_cast<vtkIdType>(grid->h));
    for (vtkIdType i = 0; i < grid->w; i++)
        for (vtkIdType j = 0; j < grid->h; j++)
            table->SetValue(i, j, grid->Ez(i,j));

    // Set up the surface plot we wish to visualize and add it to the chart.
    plot->SetXRange(0, 9.424778);
    plot->SetYRange(0, 9.424778);
    plot->SetInputData(table.GetPointer());
    chart->AddPlot(plot.GetPointer());
    
    view->GetRenderWindow()->SetMultiSamples(0);
    view->GetInteractor()->Initialize();
    view->GetRenderWindow()->Render();
    
    // rotate
    vtkContextMouseEvent mouseEvent;
    mouseEvent.SetInteractor(view->GetInteractor());
    vtkVector2i pos;
    vtkVector2i lastPos;
    
    mouseEvent.SetButton(vtkContextMouseEvent::LEFT_BUTTON);
    lastPos.Set(100, 50);
    mouseEvent.SetLastScreenPos(lastPos);
    pos.Set(150, 100);
    mouseEvent.SetScreenPos(pos);
    vtkVector2d sP(pos.Cast<double>().GetData());
    vtkVector2d lSP(lastPos.Cast<double>().GetData());
    vtkVector2d screenPos(mouseEvent.GetScreenPos().Cast<double>().GetData());
    vtkVector2d lastScreenPos(mouseEvent.GetLastScreenPos().Cast<double>().GetData());
    chart->MouseMoveEvent(mouseEvent);
    view->GetInteractor()->Start();
}

void test()
{
    vtkNew<vtkChartXYZ> chart;
    vtkNew<vtkContextView> view;
    view->GetRenderWindow()->SetSize(400, 300);
    view->GetScene()->AddItem(chart.GetPointer());

    chart->SetGeometry(vtkRectf(75.0, 20.0, 250, 260));

    // Create a table with some points in it...
    vtkNew<vtkTable> table; 
    vtkNew<vtkFloatArray> arrX;
    arrX->SetName("X Axis");
    table->AddColumn(arrX.GetPointer());
    vtkNew<vtkFloatArray> arrC;
    arrC->SetName("Cosine");
    table->AddColumn(arrC.GetPointer());
    vtkNew<vtkFloatArray> arrS;
    arrS->SetName("Sine");
    table->AddColumn(arrS.GetPointer());
    vtkNew<vtkFloatArray> arrColor;
    arrColor->SetName("Color");
    table->AddColumn(arrColor.GetPointer());
    
    // Test charting with a few more points...
    int numPoints = 69;
    float inc = 7.5 / (numPoints-1);
    table->SetNumberOfRows(numPoints);
    for (int i = 0; i < numPoints; ++i) {
        table->SetValue(i, 0, i * inc);
        table->SetValue(i, 1, cos(i * inc) + 0.0);
        table->SetValue(i, 2, sin(i * inc) + 0.0);
        table->SetValue(i, 3, i);
    }

    // Add the dimensions we are interested in visualizing.
    vtkNew<vtkPlotPoints3D> plot;
    plot->SetInputData(table.GetPointer(), "X Axis", "Sine", "Cosine", "Color");
    chart->AddPlot(plot.GetPointer());

    view->GetRenderWindow()->SetMultiSamples(0);
    view->GetInteractor()->Initialize();
    view->GetRenderWindow()->Render();

    vtkContextMouseEvent mouseEvent;
    mouseEvent.SetInteractor(view->GetInteractor());
    vtkVector2i pos;
    vtkVector2i lastPos;

    // rotate
    mouseEvent.SetButton(vtkContextMouseEvent::LEFT_BUTTON);
    lastPos.Set(114, 55);
    mouseEvent.SetLastScreenPos(lastPos);
    pos.Set(174, 121);
    mouseEvent.SetScreenPos(pos);

    vtkVector2d sP(pos.Cast<double>().GetData());
    vtkVector2d lSP(lastPos.Cast<double>().GetData());

    vtkVector2d screenPos(mouseEvent.GetScreenPos().Cast<double>().GetData());
    vtkVector2d lastScreenPos(mouseEvent.GetLastScreenPos().Cast<double>().GetData());
    chart->MouseMoveEvent(mouseEvent);

    // spin
    mouseEvent.SetButton(vtkContextMouseEvent::LEFT_BUTTON);
    mouseEvent.GetInteractor()->SetShiftKey(1);
    lastPos.Set(0, 0);
    mouseEvent.SetLastScreenPos(lastPos);
    pos.Set(10, 10);
    mouseEvent.SetScreenPos(pos);
    chart->MouseMoveEvent(mouseEvent);

    // zoom
    mouseEvent.SetButton(vtkContextMouseEvent::RIGHT_BUTTON);
    mouseEvent.GetInteractor()->SetShiftKey(0);
    lastPos.Set(0, 0);
    mouseEvent.SetLastScreenPos(lastPos);
    pos.Set(0, 10);
    mouseEvent.SetScreenPos(pos);
    chart->MouseMoveEvent(mouseEvent);

    // mouse wheel zoom
    chart->MouseWheelEvent(mouseEvent, -1);

    // pan
    mouseEvent.SetButton(vtkContextMouseEvent::RIGHT_BUTTON);
    mouseEvent.GetInteractor()->SetShiftKey(1);
    lastPos.Set(10, 10);
    mouseEvent.SetLastScreenPos(lastPos);
    pos.Set(0, 0);
    mouseEvent.SetScreenPos(pos);
    chart->MouseMoveEvent(mouseEvent);

    // remove colors
    plot->SetInputData(table.GetPointer(), "X Axis", "Sine", "Cosine");
    view->GetRenderWindow()->Render();

    // add them back in
    plot->SetColors(arrColor.GetPointer());

    view->GetInteractor()->Start();
}

// void vtk_line_plot(System *sys, Grid *grid)
// {
//     // Create a table with some points in it
//   vtkSmartPointer<vtkTable> table = 
//     vtkSmartPointer<vtkTable>::New();
//  
//   vtkSmartPointer<vtkFloatArray> arrX = 
//     vtkSmartPointer<vtkFloatArray>::New();
//   arrX->SetName("X Axis");
//   table->AddColumn(arrX);
//  
//   vtkSmartPointer<vtkFloatArray> arrC = 
//     vtkSmartPointer<vtkFloatArray>::New();
//   arrC->SetName("Cosine");
//   table->AddColumn(arrC);
//  
//   vtkSmartPointer<vtkFloatArray> arrS = 
//     vtkSmartPointer<vtkFloatArray>::New();
//   arrS->SetName("Sine");
//   table->AddColumn(arrS);
//  
//   // Fill in the table with some example values
//   int numPoints = 69;
//   float inc = 7.5 / (numPoints-1);
//   table->SetNumberOfRows(numPoints);
//   for (int i = 0; i < numPoints; ++i)
//   {
//     table->SetValue(i, 0, i * inc);
//     table->SetValue(i, 1, cos(i * inc));
//     table->SetValue(i, 2, sin(i * inc));
//   }
//  
//   // Set up the view
//   vtkSmartPointer<vtkContextView> view = 
//     vtkSmartPointer<vtkContextView>::New();
//   view->GetRenderer()->SetBackground(1.0, 1.0, 1.0);
//  
//   // Add multiple line plots, setting the colors etc
//   vtkSmartPointer<vtkChartXY> chart = 
//     vtkSmartPointer<vtkChartXY>::New();
//   view->GetScene()->AddItem(chart);
//   vtkPlot *line = chart->AddPlot(vtkChart::LINE);
// #if VTK_MAJOR_VERSION <= 5
//   line->SetInput(table, 0, 1);
// #else
//   line->SetInputData(table, 0, 1);
// #endif
//   line->SetColor(0, 255, 0, 255);
//   line->SetWidth(1.0);
//   line = chart->AddPlot(vtkChart::LINE);
// #if VTK_MAJOR_VERSION <= 5
//   line->SetInput(table, 0, 2);
// #else
//   line->SetInputData(table, 0, 2);
// #endif
//   line->SetColor(255, 0, 0, 255);
//   line->SetWidth(5.0);
//  
//   // For dotted line, the line type can be from 2 to 5 for different dash/dot
//   // patterns (see enum in vtkPen containing DASH_LINE, value 2):
// #ifndef WIN32
//   line->GetPen()->SetLineType(vtkPen::DASH_LINE);
// #endif
//   // (ifdef-ed out on Windows because DASH_LINE does not work on Windows
//   //  machines with built-in Intel HD graphics card...)
//  
//   //view->GetRenderWindow()->SetMultiSamples(0);
//  
//   // Start interactor
//   view->GetInteractor()->Initialize();
//   view->GetInteractor()->Start();
// }

void vtk_ez(System *sys, Grid *grid) 
{
    std::cout << "[" << "\033[1;32m" << "*" << "\033[0m" << "] ";
    std::cout << "Running VTK --> Ez" << std::endl;
    
    vtkSmartPointer<vtkDoubleArray> efield = vtkSmartPointer<vtkDoubleArray>::New();
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    vtkSmartPointer<vtkStructuredGrid> structuredGrid = vtkSmartPointer<vtkStructuredGrid>::New();

    efield->SetNumberOfComponents(3);
    efield->SetName("efield");

    // // Shift all values to above zero.
    // double min_val = grid->Ez.min();
    // grid->Ez = grid->Ez + fabs(min_val);
    // 
    // // Now normalize all values in the Ez matrix.
    // double max_val = grid->Ez.max();
    // grid->Ez = grid->Ez / fabs(max_val);
    // 
    // // Add a small number to each element to overcome the zero error in log scale
    // grid->Ez = grid->Ez + 0.00001;
    // grid->Ez.print("Ez:");
    
    for (int i = 0; i < grid->w; i++) {
        for (int j = 0; j < grid->h; j++) {
            double r = grid->Ez(i,j);
            double g = 0.0;
            double b = 0.0;
    
            double vertex[3] = {r, g, b};
    
            points->InsertNextPoint(i, j, 0);
            efield->InsertNextTupleValue(vertex);
        }
    }
    
    structuredGrid->SetDimensions(grid->w, grid->h, 1);
    structuredGrid->SetPoints(points);
    structuredGrid->GetPointData()->SetVectors(efield);
    
    vtkSmartPointer<vtkXMLStructuredGridWriter> writer = vtkSmartPointer<vtkXMLStructuredGridWriter>::New();
    writer->SetFileName("ez.vts");
    writer->SetInputData(structuredGrid);
    writer->Write();
}













