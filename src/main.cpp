#include "sys.h"
#include "sources.h"
#include "update.h"
#include "filesystem.h"
#include "grid.h"
#include "vtk_tools.h"
#include "abc.h"
#include "tfsf.h"
#include "json.hpp"

#include <docopt/docopt.h>
#include <cstdlib>
#include <math.h>

static const char *const USAGE =
    R"(Usage: 
        nora <circuit-name> [--grid <x> <y>] [--time <t>]

        Options:
            -v --verbose
            -d --debug
            -h --help
            --version
)";

// https://github.com/nlohmann/json
using json = nlohmann::json;

void write_position_analysis(System *sys, Grid *g)
{
    std::vector<double> x;
    std::vector<double> Ez_pos;
    
    for (int i = 0; i < g->w; i++) {
        x.push_back(i);
        Ez_pos.push_back(g->Ez(i,50));
    }
    
    json jsonMain;
    
    jsonMain["x"] = x;
    jsonMain["Ez_pos"] = Ez_pos;
    
    std::ofstream json_file("ricker.json");
    json_file << std::setw(4) << jsonMain << std::endl;
}

void write_time_analysis(System *sys, Grid *g, Sources *source)
{
    std::vector<double> t;
    std::vector<double> Ez_time;
    
    for (int i = 0; i < sys->maxTime; i++) {
        t.push_back(i);
        Ez_time.push_back(source->ricker(i));
    }
    
    json jsonMain;
    
    jsonMain["t"] = t;
    jsonMain["Ez_time"] = Ez_time;
    
    std::ofstream json_file("ricker.json");
    json_file << std::setw(4) << jsonMain << std::endl;
}

void write_ez_zero(System *sys, Grid *g, std::vector<double> Ez0)
{
    std::vector<double> t;
    
    for (int i = 0; i < 100; i++)
        t.push_back(i);
    
    json jsonMain;
    
    jsonMain["t"] = t;
    jsonMain["Ez0"] = Ez0;
    
    std::ofstream json_file("ricker.json");
    json_file << std::setw(4) << jsonMain << std::endl;
}

int main(int argc, char *argv[])
{
    System *sys = new System;
    Sources *source = new Sources;
    Abc *abc = new Abc;
    Grid *g = new Grid;
    Grid *g1 = new Grid;
    Tfsf *tfsf = new Tfsf;

    sys->maxTime = 100;
    
    std::string version = "Nora 1.0.0";

    std::cout << "\n";
    std::cout << "[" << "\033[1;32m" << "*" << "\033[0m" << "] ";
    std::cout << "Parameters:" << std::endl;
    auto args = docopt::docopt(USAGE, {argv+1, argv+argc}, true, version, false);
    for(auto const& arg : args)
        std::cout << "    " << arg.first << ": " << arg.second << std::endl;
    std::cout << "\n";

    if (args["--grid"].asBool()) {
        std::cout << "\n";
        std::cout << "[" << "\033[1;32m" << "*" << "\033[0m" << "] ";
        std::cout << "Updated grid dimensions to: ";

        if (args["<x>"].isString())
            g->w = std::stoi(args["<x>"].asString());
        if (args["<y>"].isString())
            g->h = std::stoi(args["<y>"].asString());

        std::cout << g->w << " " << g->h << std::endl;
    }
    
    // g1->time = 0;
    // g1->init_grid_1D();
    
    g->init_grid();
    source->init_source(sys);
    // abc->abc_init(sys, g);
    // tfsf->tfsf_init(sys, g);
    
    // std::cout << g->type << " : " << g1->type << std::endl;

    if (args["--time"].asBool()) {
        std::cout << "[" << "\033[1;32m" << "*" << "\033[0m" << "] ";
        std::cout << "Updated run time to: ";

        if (args["<t>"].isString())
            sys->maxTime = std::stoi(args["<t>"].asString());

        std::cout << sys->maxTime << std::endl;
    }
    
    std::vector<double> Ez0;
    
    if (args["<circuit-name>"].isString()) {
        std::string circuit_name = args["<circuit-name>"].asString();
        if (circuit_name.compare("free") == 0) {
            std::cout << "[" << "\033[1;32m" << "*" << "\033[0m" << "] ";
            std::cout << "Solving FDTD in free space" << std::endl;
            
            // file_system(sys, circuit_name);
            
            for (int i = 0; i < sys->maxTime; i++) {
                updateH(sys, g, g1);
                // tfsf->tfsf_update(sys, g, g1, source);
                updateE(sys, g, g1);
                // abc->abc(sys, g);
                
                int wh = g->w / 2;
                int hh = g->h / 2;

                g->Ez(wh, hh) = source->ricker_wave(i, 0.0);
                
                if (i < 100) {
                    std::cout << g->Ez(50, 51) << std::endl;
                    Ez0.push_back(g->Ez(50, 55));
                }
                
                if (i == 70) {
                    // write_position_analysis(sys, g);
                    vtk_ez(sys, g);
                }
            }
        }
    }
    
    write_ez_zero(sys, g, Ez0);
    // write_time_analysis(sys, g, source);
    
    std::cout << "\nNora: " << "\033[1;31m" << "Success" << "\033[0m" << "\n\n";

    return 0;
}














