#include "grid.h"
#include "globals.h"

Grid::Grid() 
{
    w = 101;
    h = 101;
}

Grid::~Grid() {}

void Grid::init_grid()
{
    type = 2;
    
    // double cdtds = 1.0 / std::sqrt(2.0);
    
    Hx.resize(w, h-1);
    Hy.resize(w-1, h);
    Ez.resize(w, h);
    
    Hx.fill(0.0);
    Hy.fill(0.0);
    Ez.fill(0.0);
    
    Ceze.resize(w, h);
    Cezh.resize(w, h);

    Chxe.resize(w, h-1);
    Chxh.resize(w, h-1);

    Chye.resize(w-1, h);
    Chyh.resize(w-1, h);
    
    Ceze.fill(0.0);
    Cezh.fill(0.0);
    
    Chxe.fill(0.0);
    Chxh.fill(0.0);
    
    Ceze.fill(0.0);
    Cezh.fill(0.0);
    
    // Set electric field coefficients
    for (int i = 0; i < w; i++) {
        for (int j = 0; j < h; j++) {
            Ceze(i,j) = 1.0;
            // Cezh(i,j) = IMP0;
            Cezh(i,j) = CDTDS * IMP0;
        }
    }
    
    // Set magnetic field coefficients
    for (int i = 0; i < w; i++) {
        for (int j = 0; j < h-1; j++) {
            Chxh(i,j) = 1.0;
            // Chxe(i,j) = 1.0 / IMP0;
            Chxe(i,j) = CDTDS / IMP0;
        }
    }
    
    for (int i = 0; i < w-1; i++) {
        for (int j = 0; j < h; j++) {
            Chyh(i,j) = 1.0;
            // Chye(i,j) = 1.0 / IMP0;
            Chye(i,j) = CDTDS / IMP0;
        }
    }
}

void Grid::init_grid_1D()
{
    double depth_in_layer = 0.0;
    double loss_factor = 0.0;
    
    w += NLOSS;
    type = 1;
    
    Hx1D.resize(w);
    Hy1D.resize(w);
    Ez1D.resize(w);
    
    Ceze1D.resize(w-1);
    Cezh1D.resize(w-1);
    
    Chxe1D.resize(w-1);
    Chxh1D.resize(w-1);
    
    Chye1D.resize(w-1);
    Chyh1D.resize(w-1);
    
    for (int i = 0; i < w-1; i++) {
        if (i < w-1-NLOSS) {
            Ceze1D(i) = 1.0;
            Cezh1D(i) = CDTDS * IMP0;
            Chyh1D(i) = 1.0;
            Chye1D(i) = CDTDS * IMP0;
        } else {
            depth_in_layer = i - (w-1-NLOSS) + 0.5;
            loss_factor = MAX_LOSS + powf(depth_in_layer / NLOSS, 2);
            Ceze1D(i) = (1.0 - loss_factor) / (1.0 + loss_factor);
            Cezh1D(i) = CDTDS * IMP0 / (1.0 + loss_factor);
            
            depth_in_layer += 0.5;
            loss_factor = MAX_LOSS * powf(depth_in_layer / NLOSS, 2);
            Chyh1D(i) = (1.0 - loss_factor) / (1.0 + loss_factor);
            Chye1D(i) = CDTDS / IMP0 / (1.0 + loss_factor);
        }
    }
}






























