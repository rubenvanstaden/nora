#include "abc.h"

Abc::Abc() {}
Abc::~Abc() {}

void Abc::abc_init(System *sys, Grid *g)
{
    init_done = 1;
    double temp1, temp2;
        
    // Calculate ABC coefficients.
    temp1 = sqrtf(g->Cezh(0,0) * g->Chye(0,0));
    temp2 = 1.0 / temp1 + 2.0 + temp1;
    coef0 = -(1.0 / temp1 - 2.0 + temp1) / temp2;
    coef1 = -2.0 * (temp1 - 1.0 / temp1) / temp2;
    coef2 = 4.0 * (temp1 + 1.0 / temp1) / temp2;
    
    EzLeft_1.resize(g->w, g->h);
    EzRight_1.resize(g->w, g->h);
    EzTop_1.resize(g->h, g->w);
    EzBottom_1.resize(g->h, g->w);
    
    EzLeft_2.resize(g->w, g->h);
    EzRight_2.resize(g->w, g->h);
    EzTop_2.resize(g->h, g->w);
    EzBottom_2.resize(g->h, g->w);
}

void Abc::abc(System *sys, Grid *g)
{
    // ABC at left side of grid.
    for (int i = 0; i < g->h; i++) {
        g->Ez(0,i) = coef0 * (g->Ez(2,i) + EzLeft_2(0,i)) + 
                     coef1 * (EzLeft_1(0,i) + EzLeft_1(2,i) - g->Ez(1,i) - EzLeft_2(1,i)) +
                     coef2 * EzLeft_1(1,i) - EzLeft_2(2,i);
                        
        // Memorize old fields.
        for (int j = 0; j < 3; j++) {
            EzLeft_2(j,i) = EzLeft_1(j,i);
            EzLeft_1(j,i) = g->Ez(j,i);
        }
    }

    // ABC at right side of grid.
    for (int j = 0; j < g->h; j++) {
        g->Ez(g->w-1, j) = coef0 * (g->Ez(g->w - 3, j) + EzRight_2(0,j)) + 
                           coef1 * (EzRight_1(0,j) + EzRight_1(2,j) - g->Ez(g->w-2, j) - EzRight_2(1,j)) +
                           coef2 * EzRight_1(1,j) - EzRight_2(2,j);

        // Memorize old fields.
        for (int i = 0; i < 3; i++) {
            EzRight_2(i,j) = EzRight_1(i,j);
            EzRight_1(i,j) = g->Ez(g->w-1-i, j);
        }
    }
    
    // ABC at bottom of grid.
    for (int i = 0; i < g->w; i++) {
        g->Ez(i,0) = coef0 * (g->Ez(i,2) + EzBottom_2(0,i)) + 
                     coef1 * (EzBottom_1(0,i) + EzBottom_1(2,i) - g->Ez(i,1) - EzBottom_2(1,i)) + 
                     coef2 * EzBottom_1(1,i) - EzBottom_2(2,i);
                        
        // Memorize old fields.
        for (int j = 0; j < 3; j++) {
            EzBottom_2(j,i) = EzBottom_1(j,i);
            EzBottom_1(j,i) = g->Ez(i,j);
        }
    }
    
    // ABC at top of grid.
    for (int i = 0; i < g->w; i++) {
        g->Ez(i, g->h-1) = coef0 * (g->Ez(i, g->h-3) + EzTop_2(0,i)) + 
                           coef1 * (EzTop_1(0,i) + EzTop_1(2,i) - g->Ez(i, g->h-2) - EzTop_1(2,i)) + 
                           coef2 * EzTop_1(1,i) - EzTop_2(2,i);
                               
        // Memorize old fields.
        for (int j = 0; j < 3; j++) {
            EzTop_2(j,i) = EzTop_1(j,i);
            EzTop_1(j,i) = g->Ez(i, g->h-1-j);
        }
    }
}































