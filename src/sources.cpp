#include "sources.h"

Sources::Sources() 
{
    ppw = 20.0;
}

Sources::~Sources() {}

void Sources::init_source(System *sys)
{
    ricker.resize(sys->maxTime);
}

double Sources::ricker_wave(int t, double location)
{
    // double value = sin(2.0 * PI / ppw * ((CDTDS * (t/500)) - location));
    // 
    // double td = (double)(t/300.0);
    // double value = exp(-(td + 0.5 - (-0.5) - 30.) * (td + 0.5 - (-0.5) - 30.) / 100.);
    
    // double cdtds = 1.0 / std::sqrt(2.0);
    // 
    double td = (double)(t);
    double arg = PI * (((CDTDS * td - location) / 20.0) - 1.0);
    arg = arg * arg;
    double value = (1.0 - 2.0 * arg) * exp(-arg);
    
    ricker(t) = value;
    
    return value;
}