#include "tfsf.h"

Tfsf::Tfsf() {}
Tfsf::~Tfsf() {}

void Tfsf::tfsf_init(System *sys, Grid *g)
{    
    firstX = 5.0;
    firstY = 5.0;
    lastX = 95.0;
    lastY = 95.0;
    
    // ez_inc_init(g);
}

void Tfsf::tfsf_update(System *sys, Grid *g, Grid *g1, Sources *source)
{
    // std::cout << "[" << "\033[1;32m" << "*" << "\033[0m" << "] ";
    // std::cout << "Updating TFSF boundary" << std::endl;
    
    int i, j;
    
    // Correct Hy along left edge.
    i = firstX - 1;
    for (j = firstY; j <= lastY; j++) {
        g->Hy(i,j) -= g->Chye(i,j) * g1->Ez1D(i+1);
    }
    
    // Correct Hy along right edge.
    i = lastX;
    for (j = firstY; j <= lastY; j++) {
        g->Hy(i,j) += g->Chye(i,j) * g1->Ez1D(i);
    }
    
    // Correct Hx along the bottom.
    j = firstY - 1;
    for (i = firstX; i <= lastX; i++) {
        g->Hx(i,j) += g->Chxe(i,j) * g1->Ez1D(i);
    }
    
    // Correct Hx along the top.
    j = lastY;
    for (i = firstX; i <= lastX; i++) {
        g->Hx(i,j) -= g->Chxe(i,j) * g1->Ez1D(i);
    }
    
    updateH(sys, g, g1);
    updateE(sys, g, g1);
    g1->Ez1D(0) = source->ricker_wave(g1->time, 0.0);
    // std::cout << g1->time << std::endl;
    g1->time++;
    
    // =========================================================================
    // Correct Ez adjacent to TFSF boundary.
    // =========================================================================

    // Correct Ez field along left edge.
    i = firstX;
    for (j = firstY; j <= lastY; j++) {
        g->Ez(i,j) -= g->Cezh(i,j) * g1->Hy1D(i-1);
    }
    
    // Correct Ez field along right edge.
    i = lastX;
    for (j = firstY; j <= lastY; j++) {
        g->Ez(i,j) += g->Cezh(i,j) * g1->Hy1D(i);
    }
}








